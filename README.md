In an effort to future-proof the Mossbauer spectroscopy calibration and the statistical analysis presented in IJQC 2020, we constructed this database as a tool for the prediction and rigorous statistical analysis of computed Mössbauer parameters. \
To directly access the interactive notebook, go to https://tinyurl.com/moessbauer, click on 'MossbauerPrediction.ipynb' and follow the instructions.

The database is publicly accessible and open to submissions from other researchers. It can be used in at least three ways: 
1.	Obtaining a predicted isomer shift or quadrupole splitting including the associated uncertainty estimates simply by typing in the computed contact density or quadrupole splitting.  
2.	Submitting reference data points for additional complexes to obtain more rigorous statistics; the data points will be reviewed by the authors.
3.	Obtaining complete statistical analyses by submitting new data sets computed with a different computational setup, for example, DFT with different functionals, basis sets, solvation models, relativistic corrections, and so on, or a post‐HF approach; the data sets will be validated by the authors. 

Please cite the original publication if you make use of this notebook: C. Gallenkamp, U. I. Kramm, J. Proppe, V. Krewald, Int. J. Quantum Chem. 2020, accepted.

The ORCA input file for the Moessbauer calculations used in this paper is pasted below. For information on the individual keywords, please refer to the ORCA manual.
To run calculations, replace FUNCTIONAL by B3LYP/TPSSh/PBE0, insert the coordinates in the coordinates block and provide the correct charge (C) and multiplicity (M). 

! UKS FUNCTIONAL RIJCOSX def2-TZVP def2/J D3BJ \
! TightSCF Grid6 NoFinalGrid GridX8 NoFinalGridX \
! KDIIS Slowconv CPCM(water) PrintMOs

%basis
        newGTO Fe "CP(PPP)" end \
end


%method intacc 6.0 \
        SpecialGridAtoms 26 \
        SpecialGridIntacc 7.0 \
end

%scf MaxIter 500 \
     shift shift 1.0 erroff 0.2 end \
end

%cpcm smd true \
      smdsolvent "water" \
end

*xyz C M \
...\
*

%eprnmr nuclei = all Fe {fgrad, rho} \
end
